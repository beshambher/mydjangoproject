from django.conf.urls import include, url

from django.contrib import admin
admin.autodiscover()

import hello.views

from accounts.views import *

# Examples:
# url(r'^$', 'gettingstarted.views.home', name='home'),
# url(r'^blog/', include('blog.urls')),

urlpatterns = [
    #url(r'^$', hello.views.index, name='index'),
    url(r'^db', hello.views.db, name='db'),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^$|^home/', homepage, name="home"),

    url(r'^accounts/', include('allauth.urls')),

    url('^login/', login_view, name="login"),
    url('^logout/', logout_view, name="logout"),
    url('^register/', register_view, name="register"),
    url('^profile/', profile_view, name="profile"),
]
